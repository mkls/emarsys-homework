# README #

Homework exercise

Example
```javascript
const calculateDueDate = require('./index')

const dueDate = calculateDueDate(new Date('2017-03-21T14:22'), 16)

console.log(dueDate)    // 2017-03-23T14:22:00.000Z
```

To start development:
```
npm i
npm run tdd
```