const ONE_HOUR = 60 * 60 * 1000
const EIGHT_HOURS = 8 * ONE_HOUR
const ONE_DAY = 24 * ONE_HOUR

const calculateDueDate = (submitDate, turnAroundTime) => {
    submitDate = submitDate.getTime()
    turnAroundTime *= ONE_HOUR

    const submitDateDayStart = new Date(submitDate).setHours(9, 0, 0, 0)
    const timeLeftCurrentDate = EIGHT_HOURS - (submitDate - submitDateDayStart);

    const dayOverlappingTime = turnAroundTime - timeLeftCurrentDate;

    if (dayOverlappingTime < 0) {
        return new Date(submitDate + turnAroundTime)
    }

    const overLappingDays = Math.floor(dayOverlappingTime / EIGHT_HOURS)
    const leftOverTime = dayOverlappingTime - overLappingDays * EIGHT_HOURS;

    const compensatedDays = addWeekendDays(overLappingDays + 1, new Date(submitDate).getDay())

    return new Date(submitDateDayStart + compensatedDays * ONE_DAY + leftOverTime)
}

const addWeekendDays = (days, startDayOfWeek) => {
    const weekDays = startDayOfWeek + days
    const weekends = Math.floor(weekDays / 5)

    return weekDays + weekends * 2 - startDayOfWeek
}

module.exports = {
    calculateDueDate,
    addWeekendDays
}
