const test = require('tape')
const calculateDueDate = require('./due-date').calculateDueDate
const addWeekendDays = require('./due-date').addWeekendDays

test('zero turnaround time', t => {
    var actual = calculateDueDate(
        new Date('Tue Mar 14 2017 14:22:00'),
        0
    )
    var expected = new Date('Thu Mar 14 2017 14:22:00')

    t.equal(actual.toString(), expected.toString())
    t.end()
})

test('time without day overlap', t => {
    var actual = calculateDueDate(
        new Date('Tue Mar 14 2017 14:22:00'),
        1
    )
    var expected = new Date('Thu Mar 14 2017 15:22:00')

    t.equal(actual.toString(), expected.toString())
    t.end()
})

test('one hour with day overlap', t => {
    var actual = calculateDueDate(
        new Date('Wed Mar 15 2017 16:58:59'),
        1
    )
    var expected = new Date('Thu Mar 16 2017 9:58:59')

    t.equal(actual.toString(), expected.toString())
    t.end()
})

test('two days overlap', t => {
    var actual = calculateDueDate(
        new Date('Tue Mar 14 2017 14:22:00'),
        16
    )
    var expected = new Date('Thu Mar 16 2017 14:22:00')

    t.equal(actual.toString(), expected.toString())
    t.end()
})

test('weekend overlap', t => {
    var actual = calculateDueDate(
        new Date('Fri Mar 17 2017 14:00:00'),
        6
    )
    var expected = new Date('Mon Mar 20 2017 12:00:00')

    t.equal(actual.toString(), expected.toString())
    t.end()
})

test('weekend overlap multipe days', t => {
    var actual = calculateDueDate(
        new Date('Fri Mar 17 2017 14:00:00'),
        16
    )
    var expected = new Date('Tue Mar 21 2017 14:00:00')

    t.equal(actual.toString(), expected.toString())
    t.end()
})

test('long and complex turnaround time', t => {
    var actual = calculateDueDate(
        new Date('Fri Mar 10 2017 16:01:00'),
        8 * 5 + 8 + 2
    )
    var expected = new Date('Tue Mar 21 2017 10:01:00')

    t.equal(actual.toString(), expected.toString())
    t.end()
})

test.skip('turnaround over a daylight saving date (mar 26)', t => {
    var actual = calculateDueDate(
        new Date('Fri Mar 24 2017 16:01:00'),
        8 * 5 + 8 + 2
    )
    var expected = new Date('Mon Mar 27 2017 10:01:00')

    t.equal(actual.toString(), expected.toString())
    t.end()
})


test('addWeekendDays', t => {
    t.equal(
        addWeekendDays(0, 4),
        0,
        'adding to zero days'
    )

    t.equal(
        addWeekendDays(2, 2),
        2,
        'no weekend included'
    )

    t.equal(
        addWeekendDays(1, 5),
        3,
        'weekend overlap'
    )

    t.equal(
        addWeekendDays(7, 5),
        11,
        'double weekend overlap'
    )

    t.equal(
        addWeekendDays(10, 4),
        14,
        'multiple weekend overlap'
    )

    t.equal(
        addWeekendDays(100, 1),
        140,
        'very many weekend overlap'
    )
    t.end()
})
